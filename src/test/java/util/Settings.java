package util;

import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Settings {

    public static WebDriver driver;


    @Rule
    public TestRule printTests = new TestWatcher() {
        protected void starting(Description description) {
            System.out.println("\n" + "( ͡° ͜ʖ ͡°) Start Test: " + description.getMethodName());
        }

        protected void finished(Description description) {
            System.out.println("( ͡° ͜ʖ ͡°) Finish Test: " + description.getMethodName());
        }
    };


    protected static void uploadApp(){

        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("marionette", false);
        driver = new FirefoxDriver(capabilities);

        driver.get("http://whatscook.ru/#/");
    }
}

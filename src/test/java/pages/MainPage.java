package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage {
    private WebDriver driver;

    /**
     * Locators
     */
    private By cook_button = By.xpath("/html/body/div/div/div[2]/div[2]/button");
    private By header_main_page = By.xpath("/html/body/div/div/div[2]/div[2]/p[2]");
    private By header_instruction_page = By.xpath("/html/body/div/div/div[2]/div[2]/div/div/div[3]");
    private By recipe_page = By.xpath("/html/body/div/div/div[2]/div[2]/div");
    private By list_ingridients = By.xpath("/html/body/div/div/div[2]/div[2]/div/div/div[4]/div[2]");
    private By cook_instruction = By.xpath("/html/body/div/div/div[2]/div[2]/div/div/div[5]/ol");
    private By btn_close = By.xpath("/html/body/div/div/div[2]/div[2]/div/div/div[1]");


    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Methods
     */
    public void clickOnButtonCook() {
        driver.findElement(cook_button).click();
    }

    public String getHeaderMine(){
        return driver.findElement(header_main_page).getText();
    }

    public String getHeaderInstruction(){
        return driver.findElement(header_instruction_page).getText();
    }

    public String getHeaderMainWithoutLastSymbol(){
        return getHeaderMine().substring(0, getHeaderMine().length()-1);
    }

    public boolean isVisibbleInstructionPage(){
        return driver.findElement(recipe_page).isDisplayed();
    }

    public int getSizeListIngridient(){
        return driver.findElement(list_ingridients).findElements(By.className("ingridient")).size();
    }

    public int getSizeListInstruction(){
        return driver.findElement(cook_instruction).findElements(By.xpath("*")).size();
    }

    public void clickOnButtonClose(){
        driver.findElement(btn_close).click();
    }
}

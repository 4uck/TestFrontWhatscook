import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import pages.MainPage;
import util.Settings;


public class WebInterface extends Settings{

    private static MainPage mainPage;

    @BeforeClass
    public static void setUp(){
        uploadApp();
        mainPage = new MainPage(driver);
    }


    @Test
    public void test_succes_open_recipe_page(){
        mainPage.clickOnButtonCook();

        Assert.assertTrue(mainPage.isVisibbleInstructionPage());
    }

    @Test
    public void test_succes_study_page_recipe() {

        mainPage.clickOnButtonCook();

        String headerMineWithoutlastSymbol = mainPage.getHeaderMainWithoutLastSymbol();

        Assert.assertTrue(headerMineWithoutlastSymbol.equals(mainPage.getHeaderInstruction()));

        Assert.assertTrue(mainPage.getSizeListIngridient() > 2);

        Assert.assertTrue(mainPage.getSizeListInstruction() > 2);
    }

    @Test
    public void test_succes_close_instruction_page(){
        mainPage.clickOnButtonCook();

        Assert.assertTrue(mainPage.isVisibbleInstructionPage());

        mainPage.clickOnButtonClose();

        Assert.assertTrue(!mainPage.isVisibbleInstructionPage());
    }

    @AfterClass
    public static void tearDown(){
        driver.quit();
    }

}
